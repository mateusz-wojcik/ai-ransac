import subprocess

import cv2
import numpy as np
from timeit import default_timer as timer

import utils
from Heuristic import RandomHeuristic, DistanceHeuristic, ProbabilityHeuristic, ReductionHeuristic, NeighbourHeuristic
from KeyPoint import KeyPoint
from Ransac import Ransac
from Transformation import AffineTransformation, PerspectiveTransformation

image_name = "kubek1.png"
image2_name = "kubek2.png"


def get_image_path(image_name, full_path):
    return "/home/mateusz/PycharmProjects/ai-ransac/images/" + image_name if full_path else "../images/" + image_name


def extract_features(image_name, visualize):
    command = "cd feature_extractor/ && ./extract_features_64bit.ln -haraff -sift -i " + get_image_path(image_name, False)
    if visualize:
        command += " -DE"
    subprocess.call(command, shell=True)


def parse_line_to_point(line):
    parsed = line.split()
    features = np.array([float(f) for f in parsed[2:]])
    return KeyPoint(float(parsed[0]), float(parsed[1]), features)


# features_count, key_point_count, features
def parse_haraff_file(image_name):
    counts = []
    key_points = []
    with open("images/" + image_name + ".haraff.sift") as file:
        for line in file:
            if len(line.split()) == 1:
                counts.append(int(line))
            else:
                key_points.append(parse_line_to_point(line))
    return counts[0], counts[1], key_points


def get_key_points_pairs(key_points_1, key_points_2):
    all_pairs = []
    result = []

    def get_key_points_pairs_help(kp_1, kp_2, rev):
        for i in range(len(kp_1)):
            min_distance = np.inf
            nearest_neighbour = None
            key_point_1 = kp_1[i]
            for j in range(len(kp_2)):
                distance = key_point_1.neighbourhood_distance_to(kp_2[j])
                if distance < min_distance:
                    min_distance = distance
                    nearest_neighbour = kp_2[j]
            if rev and (nearest_neighbour, key_point_1) in all_pairs:
                nearest_neighbour.set_friend(key_point_1)
                result.append((nearest_neighbour, key_point_1))
            elif not rev:
                all_pairs.append((key_point_1, nearest_neighbour))

    get_key_points_pairs_help(key_points_1, key_points_2, False)
    get_key_points_pairs_help(key_points_2, key_points_1, True)

    return result


def get_cohesion_indicator(coherent_points, n):
    return len(coherent_points) / float(n)


def get_coherent_neighbours(point_1, point_2, n, points_1, points_2):
    neighbours, neighbours_2 = get_n_nearest_points(point_1, n, points_1), get_n_nearest_points(point_2, n, points_2)
    return [n for n in neighbours if n.friend in neighbours_2]


def get_n_nearest_points(point, n, points):
    return sorted(points, key=lambda x: point.distance_to(x))[1:n + 1]


def get_coherent_pairs(all_pairs, threshold, n):
    points_1, points_2 = [tpl[0] for tpl in all_pairs], [tpl[1] for tpl in all_pairs]
    return [pair for pair in all_pairs if get_cohesion_indicator(get_coherent_neighbours(pair[0], pair[1], n, points_1, points_2), n) >= threshold]


def visualize(img_1, img_2, keypoints_1, keypoints_2, keypoint_pairs, keypoint_coherent_pairs, keypoint_ransac, label):
    image_1 = cv2.imread(get_image_path(img_1, True))
    image_2 = cv2.imread(get_image_path(img_2, True))
    vis = np.concatenate((image_1, image_2), axis=1)
    img_name = img_1 + '_' + img_2 + '_' + label + '.png'

    offset = np.shape(image_1)[1]

    if keypoints_1 is not None:
        for point in keypoints_1:
            cv2.circle(vis, (int(point.x), int(point.y)), 2, (0, 0, 255), -1)

    if keypoints_2 is not None:
        for point in keypoints_2:
            cv2.circle(vis, (int(point.x + offset), int(point.y)), 2, (0, 0, 255), -1)

    if keypoint_pairs is not None:
        for pair in keypoint_pairs:
            cv2.line(vis, (int(pair[0].x), int(pair[0].y)), (int(pair[1].x) + offset, int(pair[1].y)), (255, 0, 0), 1)
        cv2.imwrite('pairs_' + img_name, vis)

    if keypoint_coherent_pairs is not None:
        for pair in keypoint_coherent_pairs:
            cv2.line(vis, (int(pair[0].x), int(pair[0].y)), (int(pair[1].x) + offset, int(pair[1].y)), (0, 255, 0), 1)
        cv2.imwrite('coherent_pairs_' + img_name, vis)

    if keypoint_ransac is not None:
        for pair in keypoint_ransac:
            cv2.line(vis, (int(pair[0].x), int(pair[0].y)), (int(pair[1].x) + offset, int(pair[1].y)), (0, 255, 255), 1)
        cv2.imwrite('ransac_pairs_' + img_name, vis)

    cv2.imwrite(img_name, vis)
    # cv2.imshow(img_name, vis)
    # cv2.waitKey()


def run_algorithm(cohesion_threshold, cohesion_neighbours, transform, heuristic, max_error, iterations, p=None, w=0.0):
    start = timer()

    extract_features(image_name, True)
    points = parse_haraff_file(image_name)[2]
    # utils.print_points(points)

    extract_features(image2_name, True)
    points2 = parse_haraff_file(image2_name)[2]
    # utils.print_points(points2)

    pairs = get_key_points_pairs(points, points2)
    print('Znalezione pary: ' + str(len(pairs)))

    coherent_pairs = np.array(get_coherent_pairs(pairs, cohesion_threshold, cohesion_neighbours))
    print('Pary spojne: ' + str(np.shape(coherent_pairs)[0]))
    # utils.print_points_pairs(coherent_pairs)

    # visualize(image_name, image2_name, points, points2, pairs, coherent_pairs, '')

    w = np.shape(coherent_pairs)[0] / len(pairs)
    print('w: ' + str(w))

    ransac = Ransac()
    # transform = PerspectiveTransformation()
    # heuristic = RandomHeuristic()
    pairs_ransac = None
    try:
        pairs_ransac = ransac.run(coherent_pairs, max_error, iterations, transform, heuristic, p, w)
    except ValueError:
        print("Error, ale jedziemy dalej")
    print('Pary RANSAC ' + str(len(pairs_ransac) if pairs_ransac is not None else 0))
    # utils.print_points_pairs(pairs_ransac)

    visualize(image_name, image2_name, points, points2, pairs, None, None, '')
    visualize(image_name, image2_name, None, None, None, coherent_pairs, pairs_ransac, '')

    end = timer()
    print('time: ' + str(end - start))


if __name__ == '__main__':
    transform = PerspectiveTransformation()
    heuristic = RandomHeuristic()

    # run_algorithm(0.5, 25, transform, heuristic, 15, 5000, 0.1, 0.0)
    # run_algorithm(0.5, 25, transform, heuristic, 15, 5000, 0.5, 0.0)
    run_algorithm(0.5, 25, transform, heuristic, 15, 5000, 0.7, 0.0)
    # run_algorithm(0.5, 25, transform, heuristic, 15, 5000, 0.9, 0.0)
