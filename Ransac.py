from multiprocessing.pool import Pool
from functools import partial
from Heuristic import ProbabilityHeuristic, ReductionHeuristic

import numpy as np


class Ransac:
    def run(self, pairs, max_error, iterations, transformation, heuristic=None, p=None, w=0.0):
        model = self.get_best_model(pairs, max_error, iterations, transformation, heuristic, p, w)

        if model is None:
            return None

        result_pairs = list(filter(lambda pair: self.calculate_error(pair, model) < max_error, pairs))
        return result_pairs

    def get_best_model(self, pairs, max_error, iterations, transformation, heuristic, p, w):
        best_model, best_score, worst_score = None, 0, len(pairs) + 1
        pool = Pool()

        if p is not None:
            numerator = np.log(1 - p)
            denominator = np.log(1 - np.power(w, transformation.get_points()))
            estimated_iterations = numerator / (denominator if denominator != 0 else 1e-7)

            iterations = int(round(np.minimum(iterations, estimated_iterations) if estimated_iterations > 0 else iterations))

        transformation.set_heuristic(heuristic)

        if isinstance(heuristic, ProbabilityHeuristic) or isinstance(heuristic, ReductionHeuristic):
            transformation.update_occurences(pairs, new_value=1)

        prev_score = 0
        print('Estimated iterations: ' + str(iterations))
        for i in range(iterations):
            model, score, selected_pairs = None, 0, len(pairs)
            while model is None:
                model, selected_pairs = transformation.get_model(pairs)

            # copied = partial(self.calculate_error, model=model)
            # errors = np.array(pool.map(copied, pairs))

            errors = np.array([self.calculate_error(pair, model) for pair in pairs])
            score = np.sum(errors < max_error)

            if score > best_score:
                print('Found better model ' + ' score ' + str(score) + ' iteration ' + str(i))

                if isinstance(heuristic, ProbabilityHeuristic):
                    transformation.update_occurences(selected_pairs, new_increments=10)
                elif isinstance(heuristic, ReductionHeuristic):
                    transformation.update_occurences(selected_pairs, new_value=1)

                best_score = score
                best_model = model

            if isinstance(heuristic, ReductionHeuristic) and i > 1:
                if score <= worst_score:
                    transformation.update_occurences(selected_pairs, new_value=0)
                    worst_score = score
                elif score > prev_score:
                    transformation.update_occurences(selected_pairs, 1)

            prev_score = score

        return best_model

    @staticmethod
    def calculate_error(pair, model):
        x1, y1 = pair[0].x, pair[0].y
        x2, y2 = pair[1].x, pair[1].y

        sec_matrix = np.array([x1, y1, 1.0])
        result = model @ sec_matrix.T

        t = result[2]
        u, v = result[0] / t, result[1] / t

        return np.sqrt((u - x2) ** 2 + (v - y2) ** 2)

