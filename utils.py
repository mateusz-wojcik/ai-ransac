
def print_points(points):
    for point in points:
        print(point.to_string())


def print_points_pairs(pairs):
    for pair in pairs:
        print("-----")
        print(pair[0].to_string())
        print(pair[1].to_string())
        print("-----")
