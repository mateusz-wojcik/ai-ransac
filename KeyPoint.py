import numpy as np


class KeyPoint:
    def __init__(self, x, y, features):
        self.x = x
        self.y = y
        self.features = features
        self.friend = None

    def neighbourhood_distance_to(self, key_point):
        return np.linalg.norm(self.features - key_point.features)

    def distance_to(self, other):
        return np.sqrt((np.power(self.x - other.x, 2) + np.power(self.y - other.y, 2)))

    def to_string(self):
        return str(self.x) + " " + str(self.y) + " " + str(self.features)

    def set_friend(self, friend):
        self.friend = friend

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and np.array_equal(self.features, other.features)

    def __ne__(self, other):
        return self.__eq__(other)

    def get_to_hash(self):
        yield from {
            'x': self.x,
            'y': self.y,
            'features': str(self.features)
        }.items()
